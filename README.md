
# Geodatabase Grupo de Aguas Subterráneas

**Ultima Actualización: 4 de agosto de 2021**

![Estructura de GDB, Feature datasets y feature class ](EstructuraGDB.PNG)

En el marco de la estandarización información geográfica se ha contemplado la construcción de un modelo de datos estándar auditable, útil al acopio, edición y divulgación de información geográfica con vocación hidrogeológica. Este repositorio busca reunir y centralizar el trabajo que diferentes profesionales realizan en pro de construir una Geodatabase (en adelante GDB por sus siglas en inglés) estandariza, alineada con los estándares del Servicio Geológico Colombiano, con capacidades para servir de marco de referencia en todas las actividades que realiza el **Grupo de Aguas Subterráneas**.

## Objetos geográficos

La estandarización de objetos ha contemplado la división de información en nueve *datasets* específicos, cada uno de ellos con un número determinado de capas en su interior


|          Datasets          | Número de capas |
|:--------------------------:|:---------------:|
|          Geofísica         |        1        |
|         Hidráulica         |        3        |
|        Hidrogeología       |        8        |
| Hidrogeoquímica E Isotopía |        4        |
|         Inventario         |        4        |
|   Leyenda Hidrogeológica   |        2        |
|      Recarga Acuíferos     |        9        |
|           Varios           |        2        |
|       Vulnerabilidad       |        1        |

 
Tenga en cuenta que la tabla contempla la vinculación de capas *Raster*, sin embargo la GDB que guarda el repositorio no contempla estos archivos, pues el propósito principal es **acopiar las nuevas versiones que atañen a la conformación y validación de objetos vectoriales, sus atributos y dominios.**

## Atributos y características específicas de los objetos

La versión de la *GDB* almacenada en el repositorio cuenta actualmente con 31 objetos vectoriales, sin embargo se listan también los 3 *Raster* que se almacenaran en la *GDB*.

En esta versión no se han configurado los *dominios* que codificaran algunas variables al interior de la *GDB* 

|           Dataset          |           Feature Class (Objetos)          | Geometría | Número de atributos |
|:--------------------------:|:------------------------------------------:|:---------:|:-------------------:|
|          Geofísica         |                     Ert                    |   Linea   |          20         |
|         Hidráulica         |             Dirección De Flujo             |   Linea   |          4          |
|         Hidráulica         |                  Isopiezas                 |   Linea   |          3          |
|         Hidráulica         |              Pruebas De Bombeo             |   Punto   |          30         |
|        Hidrogeología       |   Ciclo Hidrológico Perfil Hidrogeológico  |   Linea   |          2          |
|        Hidrogeología       |       Contacto Perfil Hidrogeológico       |   Linea   |          4          |
|        Hidrogeología       |   Dirección Fallas Perfil Hidrogeológico   |   Linea   |          5          |
|        Hidrogeología       |         Falla Perfil Hidrogeológico        |   Linea   |          7          |
|        Hidrogeología       |        Lineas Perfil Hidrogeológico        |   Linea   |          3          |
|        Hidrogeología       |                 Perfil Uhg                 |   Linea   |          5          |
|        Hidrogeología       |          Uhg Perfil Hidrogeológico         |  Polígono |          6          |
|        Hidrogeología       |          Unidades Hidrogeológicas          |  Polígono |          14         |
| Hidrogeoquímica E Isotopía |               Diagramas Stiff              |  Polígono |          6          |
| Hidrogeoquímica E Isotopía |                  Isolineas                 |   Linea   |          6          |
| Hidrogeoquímica E Isotopía |          Muestreo Hidrogeoquímico          |   Punto   |          27         |
| Hidrogeoquímica E Isotopía |                 Stiff Line                 |   Linea   |          2          |
|         Inventario         |                   Aljibe                   |   Punto   |          48         |
|         Inventario         |                  Manantial                 |   Punto   |          44         |
|         Inventario         |         Perforaciones Exploratorias        |   Punto   |          42         |
|         Inventario         |                    Pozo                    |   Punto   |          46         |
|   Leyenda Hidrogeológica   |               Líneas L Hidro               |   Linea   |          1          |
|   Leyenda Hidrogeológica   |         Uhg Leyenda Hidrogeológica         |  Polígono |          3          |
|      Recarga Acuíferos     |                  Cobertura                 |  Polígono |          6          |
|      Recarga Acuíferos     |                 Escorrentía                |   Linea   |          2          |
|      Recarga Acuíferos     |       Estaciones Hidroclimatológicas       |   Punto   |          15         |
|      Recarga Acuíferos     |             Evapotranspiración             |   Linea   |          2          |
|      Recarga Acuíferos     |                  Isoyetas                  |   Linea   |          2          |
|      Recarga Acuíferos     |                 Pendientes                 |   Raster  |          1          |
|      Recarga Acuíferos     |                Precipitación               |   Punto   |          4          |
|      Recarga Acuíferos     |                   Recarga                  |  Polígono |          9          |
|      Recarga Acuíferos     |               Usos Del Suelo               |  Polígono |          15         |
|           Varios           |                     Dem                    |   Raster  |          1          |
|           Varios           |                 Isovalores                 |   Raster  |          1          |
|       Vulnerabilidad       | Vulnerabilidad Intrínseca A La Contaminación |  Polígono |          15         |

Con el fin de generar ediciones a mediano plazo que conduzcan al establecimiento de un modelo de datos geográficos consistente para el **Grupo de Aguas Subterráneas** se han incluido en el repositorio archivo *.shp*, vinculados al sistema de referencia coordenado *MAGNA-SIRGAS*,  cuya estructura de atributos responde fielmente a cada uno de los objetos indexados en la DGB. Adicionalmente se incluye el archivo *.xlsx*, en donde se encuentra descrita la estructura general que acompaña el diseño de la *GDB*.

